aiorestclient
=============

.. image:: https://img.shields.io/pypi/v/aiorestclient.svg
    :target: https://pypi.python.org/pypi/aiorestclient
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal.png
   :target: https://travis-ci.org/borntyping/cookiecutter-pypackage-minimal
   :alt: Latest Travis CI build status

A client library for simplifying interactions with RESTful services

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`aiorestclient` was written by `Kenny Baskett <kbaskett248@gmail.com>`_.
