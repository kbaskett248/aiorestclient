"""Defines a session for interacting with RESTful services."""

from asyncio import Semaphore, BoundedSemaphore
import inspect
import logging
from typing import Callable, Any, List, Optional, Tuple

from aiohttp import ClientSession, ClientResponse


logger = logging.getLogger(__name__)


def recursive_format(value: Any, *args, **kwargs) -> Any:
    """Recursively iterate over a value and format its contents."""
    if hasattr(value, 'items'):
        return {recursive_format(k, *args, **kwargs):
                recursive_format(v, *args, **kwargs)
                for k, v in value.items()}
    elif hasattr(value, 'format'):
        return value.format(*args, **kwargs)
    elif hasattr(value, '__iter__'):
        return [recursive_format(v, *args, **kwargs)
                for v in value]
    else:
        return value


class Session(object):
    """Represents a session to make RESTful requests.

    Attributes:
        _base_url: str: base url for all request
        session: aiohttp.ClientSession: underlying session that will be used

    Class Attributes:
        base_url: str: the base_url may be defined at the class method to
            avoid overriding the __init__ method.

    """

    base_url: str = None
    _base_url: str = None
    session: ClientSession
    params: List[Tuple[str, str]]
    semaphore: Optional[Semaphore]

    def __init__(self,
                 base_url: str=None,
                 headers=None,
                 simultaneous_requests: Optional[int]=100) -> None:
        """Initialize the object."""
        super(Session, self).__init__()
        self._base_url = base_url if base_url else self.__class__.base_url
        self.base_url = self._base_url
        self.session = ClientSession(headers=headers)
        self.params = []
        if simultaneous_requests is not None and simultaneous_requests > 0:
            self.semaphore = BoundedSemaphore(value=simultaneous_requests)

    def _make_url(self, endpoint: str) -> str:
        return '/'.join((self._base_url, endpoint))

    async def exception_handler(self, resp, *args, **kwargs) -> None:
        """Handle a request that returns a response higher than 400.

        This function receives the response, as well as any arguments passed
        to the original request function.
        """
        if resp:
            resp.raise_for_status()

    async def close(self) -> None:
        """Close the session.

        Close the underlying aiohttp.ClientSession.

        """
        await self.session.close()

    def add_params(self, **kwargs: str) -> None:
        """Add the specified params to the session."""
        self.params.extend(kwargs.items())

    def clear_params(self) -> None:
        """Add the specified params to the session."""
        self.params = []

    @classmethod
    def _make_request(
            cls,
            method: str,
            endpoint: str,
            **request_kwargs) -> Callable:
        def wrapped(func) -> Callable:
            sig = inspect.signature(func)

            async def request_handler(self, *args, **kwargs) -> Any:
                mapping = sig.bind(self, None, *args, **kwargs).arguments
                endpoint_ = endpoint.format(**mapping)

                logger.debug(
                    f"making {method} request to {self._make_url(endpoint_)}")

                req_kwargs = request_kwargs.copy()
                for k, v in req_kwargs.items():
                    if not v:
                        req_kwargs[k] = mapping.get(k, None)

                req_kwargs = recursive_format(req_kwargs, **mapping)

                if self.params:
                    new_params = self.params.copy()
                    req_params = req_kwargs['params']
                    if req_params:
                        if hasattr(req_params, 'items'):
                            new_params.extend(req_params.items())
                        else:
                            new_params.extend(req_params)
                    req_kwargs['params'] = new_params

                async with self.semaphore:
                    resp = await getattr(self, method)(endpoint_, **req_kwargs)

                if resp.status < 400:
                    logger.debug(
                        f"Processing successful response: {resp.status}")
                    return await func(self, resp, *args, **kwargs)
                else:
                    logger.debug(
                        f"Processing unsuccessful response: {resp.status}")
                    return await self.exception_handler(
                        resp, *args, **kwargs)

            return request_handler

        return wrapped

    @classmethod
    def get(cls,
            endpoint: str,
            params=None,
            data=None,
            json=None,
            headers=None) -> Callable:
        """Decorate a function to make a GET request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a GET request. If the status of the response is in the 200 range,
        the function will be called with the response. Otherwise, the class's
        exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a GET request

        """
        return cls._make_request('_get',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _get(self,
                   endpoint: str,
                   params=None,
                   data=None,
                   json=None,
                   headers=None) -> ClientResponse:
        """Make a GET request."""
        return await self.session.get(self._make_url(endpoint),
                                      params=params,
                                      data=data,
                                      json=json,
                                      headers=headers)

    @classmethod
    def post(cls,
             endpoint: str,
             params=None,
             data=None,
             json=None,
             headers=None) -> Callable:
        """Decorate a function to make a POST request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a POST request. If the status of the response is in the 200 range,
        the function will be called with the response. Otherwise, the class's
        exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a POST request

        """
        return cls._make_request('_post',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _post(self,
                    endpoint: str,
                    params=None,
                    data=None,
                    json=None,
                    headers=None) -> ClientResponse:
        """Make a POST request."""
        return await self.session.post(self._make_url(endpoint),
                                       params=params,
                                       data=data,
                                       json=json,
                                       headers=headers)

    @classmethod
    def put(cls,
            endpoint: str,
            params=None,
            data=None,
            json=None,
            headers=None) -> Callable:
        """Decorate a function to make a PUT request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a PUT request. If the status of the response is in the 200 range,
        the function will be called with the response. Otherwise, the class's
        exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a PUT request

        """
        return cls._make_request('_put',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _put(self,
                   endpoint: str,
                   params=None,
                   data=None,
                   json=None,
                   headers=None) -> ClientResponse:
        """Make a PUT request."""
        return await self.session.put(self._make_url(endpoint),
                                      params=params,
                                      data=data,
                                      json=json,
                                      headers=headers)

    @classmethod
    def delete(cls,
               endpoint: str,
               params=None,
               data=None,
               json=None,
               headers=None) -> Callable:
        """Decorate a function to make a DELETE request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a DELETE request. If the status of the response is in the 200
        range, the function will be called with the response. Otherwise, the
        class's exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a DELETE request

        """
        return cls._make_request('_delete',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _delete(self,
                      endpoint: str,
                      params=None,
                      data=None,
                      json=None,
                      headers=None) -> ClientResponse:
        """Make a DELETE request."""
        return await self.session.delete(self._make_url(endpoint),
                                         params=params,
                                         data=data,
                                         json=json,
                                         headers=headers)

    @classmethod
    def head(cls,
             endpoint: str,
             params=None,
             data=None,
             json=None,
             headers=None) -> Callable:
        """Decorate a function to make a HEAD request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a HEAD request. If the status of the response is in the 200 range,
        the function will be called with the response. Otherwise, the class's
        exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a HEAD request

        """
        return cls._make_request('_head',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _head(self,
                    endpoint: str,
                    params=None,
                    data=None,
                    json=None,
                    headers=None) -> ClientResponse:
        """Make a HEAD request."""
        return await self.session.head(self._make_url(endpoint),
                                       params=params,
                                       data=data,
                                       json=json,
                                       headers=headers)

    @classmethod
    def options(cls,
                endpoint: str,
                params=None,
                data=None,
                json=None,
                headers=None) -> Callable:
        """Decorate a function to make a OPTIONS request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make an OPTIONS request. If the status of the response is in the 200
        range, the function will be called with the response. Otherwise, the
        class's exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes an OPTIONS request

        """
        return cls._make_request('_options',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _options(self,
                       endpoint: str,
                       params=None,
                       data=None,
                       json=None,
                       headers=None) -> ClientResponse:
        """Make a OPTIONS request."""
        return await self.session.options(self._make_url(endpoint),
                                          params=params,
                                          data=data,
                                          json=json,
                                          headers=headers)

    @classmethod
    def patch(cls,
              endpoint: str,
              params=None,
              data=None,
              json=None,
              headers=None) -> Callable:
        """Decorate a function to make a PATCH request.

        This should be used as a function decorator for a function that
        processes a successful response. After decorating, the function will
        make a PATCH request. If the status of the response is in the 200
        range, the function will be called with the response. Otherwise, the
        class's exception_handler function will be called with the response.

        Args:
            endpoint: The partial url to request. The endpoint will be
                formatted with the arguments to the function before the
                request is made.
            params: A mapping for parameters to append to the URL. The
                keys and values of these arguments will be formatted
                with the arguments to the function before the request
                is made.
            data: Data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            json: Json data to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.
            headers: Headers to be sent with the request. This value will be
                formatted with the arguments to the function before the
                request is sent.

        Returns:
            A new function that makes a PATCH request

        """
        return cls._make_request('_patch',
                                 endpoint,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    async def _patch(self,
                     endpoint: str,
                     params=None,
                     data=None,
                     json=None,
                     headers=None) -> ClientResponse:
        """Make a PATCH request."""
        return await self.session.patch(self._make_url(endpoint),
                                        params=params,
                                        data=data,
                                        json=json,
                                        headers=headers)
