"""A client library for simplifying interactions with RESTful services."""


from .session import ClientResponse, Session
from aiohttp.client_exceptions import (
    ClientError,
    ClientConnectionError,
    ClientOSError,
    ClientConnectorError,
    ClientProxyConnectionError,
    ClientSSLError,
    ClientConnectorSSLError,
    ClientConnectorCertificateError,
    ServerConnectionError,
    ServerTimeoutError,
    ServerDisconnectedError,
    ServerFingerprintMismatch,
    ClientResponseError,
    ClientHttpProxyError,
    WSServerHandshakeError,
    ContentTypeError,
    ClientPayloadError,
    InvalidURL
)


__version__ = '0.1.0'
__author__ = 'Kenny Baskett <kbaskett248@gmail.com>'
__all__ = [
    ClientResponse,
    Session,
    ClientError,
    ClientConnectionError,
    ClientOSError,
    ClientConnectorError,
    ClientProxyConnectionError,
    ClientSSLError,
    ClientConnectorSSLError,
    ClientConnectorCertificateError,
    ServerConnectionError,
    ServerTimeoutError,
    ServerDisconnectedError,
    ServerFingerprintMismatch,
    ClientResponseError,
    ClientHttpProxyError,
    WSServerHandshakeError,
    ContentTypeError,
    ClientPayloadError,
    InvalidURL
]
