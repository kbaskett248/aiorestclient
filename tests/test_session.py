"""Test file for session.py."""

import asyncio
from typing import Dict, NamedTuple, Optional, Tuple

import pytest

from aiorestclient import Session, ClientResponseError


@pytest.fixture(scope='module')
def event_loop():
    """Return a module-scoped event loop."""
    return asyncio.new_event_loop()


class Github(Session):
    """Client for interacting with Github."""

    def __init__(self):
        """Initialize Github client."""
        super(Github, self).__init__('https://api.github.com')

    @Session.get('events')
    async def events(self, resp):
        """Make a GET request to the events endpoint."""
        return resp


@pytest.fixture(scope='module')
async def github():
    """Return a reference to a GitHub object."""
    session = Github()
    yield session
    await session.close()


class EchoedRequest(NamedTuple):
    """Contains request info."""

    method: str
    params: Optional[Dict]
    data: Optional[str]
    json: Optional[Dict]
    headers: Optional[Dict]

    @classmethod
    async def make(cls, resp) -> 'EchoedRequest':
        """Convert a response to an EchoedRequest object."""
        json = await resp.json()
        return cls(method=json['method'],
                   params=json.get('args', None),
                   data=json.get('data', None),
                   json=json.get('json', None),
                   headers=json.get('headers', None))


class HttpBin(Session):
    """Client for interacting with HttpBin."""

    base_url = 'https://httpbin.org'

    @Session.get('anything')
    async def echo_request_get(self, resp,
                               params=None,
                               data=None,
                               json=None,
                               headers=None) -> Tuple[int, EchoedRequest]:
        """Return the status and the echo of a GET request."""
        return resp.status, await EchoedRequest.make(resp)

    @Session.post('anything')
    async def echo_request_post(self, resp,
                                params=None,
                                data=None,
                                json=None,
                                headers=None) -> Tuple[int, EchoedRequest]:
        """Return the status and json of a POST request."""
        return resp.status, await EchoedRequest.make(resp)

    @Session.put('anything')
    async def echo_request_put(self, resp,
                               params=None,
                               data=None,
                               json=None,
                               headers=None) -> Tuple[int, EchoedRequest]:
        """Return the status and json of a PUT request."""
        return resp.status, await EchoedRequest.make(resp)

    @Session.delete('anything')
    async def echo_request_delete(self, resp,
                                  params=None,
                                  data=None,
                                  json=None,
                                  headers=None) -> Tuple[int, EchoedRequest]:
        """Return the status and json of a DELETE request."""
        return resp.status, await EchoedRequest.make(resp)

    @Session.head('anything')
    async def echo_request_head(self, resp,
                                params=None,
                                data=None,
                                json=None,
                                headers=None) -> Tuple[int, None]:
        """Return the status of a HEAD request."""
        return resp.status, None

    @Session.options('anything')
    async def echo_request_options(self, resp,
                                   params=None,
                                   data=None,
                                   json=None,
                                   headers=None) -> Tuple[int, None]:
        """Return the status of an OPTIONS request."""
        return resp.status, None

    @Session.patch('anything')
    async def echo_request_patch(self, resp,
                                 params=None,
                                 data=None,
                                 json=None,
                                 headers=None) -> Tuple[int, EchoedRequest]:
        """Return the status and json of a PATCH request."""
        return resp.status, await EchoedRequest.make(resp)

    @Session.get('status/{status}')
    async def status(self, resp, status: int) -> int:
        """Make a GET request to the status endpoint and return the status."""
        return resp.status

    @Session.get('anything', params={'fruit': '{fruit}'})
    async def get_fruit(self, resp, fruit) -> Tuple[str, Dict[str, str]]:
        """Make a GET request with the specified fruit as a param.

        Returns:
            The HTTP verb and the parameters that were echoed back.

        """
        d = await resp.json()
        return d.get('method', None), d.get('args', None)

    @Session.post('anything', params={'fruit': '{fruit}'})
    async def post_fruit(self, resp, fruit) -> Tuple[str, Dict[str, str]]:
        """Make a POST request with the specified fruit as a param.

        Returns:
            The HTTP verb and the parameters that were echoed back.

        """
        d = await resp.json()
        return d.get('method', None), d.get('args', None)

    @Session.patch('anything', params={'fruit': '{fruit}'})
    async def patch_fruit(self, resp, fruit) -> Tuple[str, Dict[str, str]]:
        """Make a PATCH request with the specified fruit as a param.

        Returns:
            The HTTP verb and the parameters that were echoed back.

        """
        d = await resp.json()
        return d.get('method', None), d.get('args', None)

    @Session.put('anything', params={'fruit': '{fruit}'})
    async def put_fruit(self, resp, fruit) -> Tuple[str, Dict[str, str]]:
        """Make a PUT request with the specified fruit as a param.

        Returns:
            The HTTP verb and the parameters that were echoed back.

        """
        d = await resp.json()
        return d.get('method', None), d.get('args', None)

    @Session.delete('anything', params={'fruit': '{fruit}'})
    async def delete_fruit(self, resp, fruit) -> Tuple[str, Dict[str, str]]:
        """Make a DELETE request with the specified fruit as a param.

        Returns:
            The HTTP verb and the parameters that were echoed back.

        """
        d = await resp.json()
        return d.get('method', None), d.get('args', None)

    async def semaphore_test(self) -> bool:
        """If the semaphore is unavailable, immediately return False."""
        if self.semaphore.locked():
            return False
        await self.echo_request_get()
        return True


@pytest.fixture(scope='module')
async def httpbin():
    """Return a reference to an HttpBin object."""
    session = HttpBin()
    yield session
    await session.close()


@pytest.mark.asyncio
async def test_get(github):
    """Test a GET against the github api."""
    resp = await github.events()
    assert resp is not None


@pytest.mark.parametrize('request_, method', [
    ('echo_request_get', 'GET'),
    ('echo_request_post', 'POST'),
    ('echo_request_put', 'PUT'),
    ('echo_request_delete', 'DELETE'),
    ('echo_request_head', 'HEAD'),
    ('echo_request_options', 'OPTIONS'),
    ('echo_request_patch', 'PATCH')
])
@pytest.mark.asyncio
async def test_request(httpbin, request_, method):
    """Test all HTTP requests against the httpbin api.

    Run a basic test to ensure the right type of request is made. Note that
    request_ is suffixed with an underscore because the request arg is used
    internally by pytest_asyncio.
    """
    status, echoed_response = await getattr(httpbin, request_)()
    assert status == 200
    if method not in ('HEAD', 'OPTIONS'):
        assert echoed_response.method == method


@pytest.mark.asyncio
async def test_error_response(httpbin):
    """Test an error response and confirm an exception is raised."""
    with pytest.raises(ClientResponseError):
        await httpbin.status(404)


@pytest.mark.parametrize('params', [
    {'key1': 'val1', 'key2': 'val2'},
    {'key': 'value'},
    {}
])
@pytest.mark.parametrize('method, verb', [
    ('echo_request_get', 'GET'),
    ('echo_request_post', 'POST'),
    ('echo_request_patch', 'PATCH'),
    ('echo_request_put', 'PUT'),
    ('echo_request_delete', 'DELETE')
])
@pytest.mark.asyncio
async def test_params(httpbin, method, verb, params):
    """Test a request with params."""
    _, echoed_response = await getattr(httpbin, method)(params=params)
    assert echoed_response.method == verb
    assert echoed_response.params == params


@pytest.mark.parametrize('fruit', [
    'apple', 'lemon', 'banana'
])
@pytest.mark.parametrize('method, verb', [
    ('get_fruit', 'GET'),
    ('post_fruit', 'POST'),
    ('patch_fruit', 'PATCH'),
    ('put_fruit', 'PUT'),
    ('delete_fruit', 'DELETE')
])
@pytest.mark.asyncio
async def test_request_params(httpbin, method, verb, fruit):
    """Test a request with request defined params."""
    received_verb, received_params = await getattr(httpbin, method)(fruit)
    assert received_verb == verb
    assert received_params == {'fruit': fruit}


@pytest.mark.parametrize('data', [
    '',
    'test data',
    'Test data\nMore test data\n'
])
@pytest.mark.parametrize('method, verb', [
    ('echo_request_get', 'GET'),
    ('echo_request_post', 'POST'),
    ('echo_request_patch', 'PATCH'),
    ('echo_request_put', 'PUT'),
    ('echo_request_delete', 'DELETE')
])
@pytest.mark.asyncio
async def test_request_data(httpbin, method, verb, data):
    """Test a request with the specified data."""
    _, echoed_response = await getattr(httpbin, method)(data=data)
    assert echoed_response.method == verb
    assert echoed_response.data == data


@pytest.mark.parametrize('json', [
    '',
    {'key': 'value'},
    {'key1': 'val1', 'key2': 'val2'}
])
@pytest.mark.parametrize('method, verb', [
    ('echo_request_get', 'GET'),
    ('echo_request_post', 'POST'),
    ('echo_request_patch', 'PATCH'),
    ('echo_request_put', 'PUT'),
    ('echo_request_delete', 'DELETE')
])
@pytest.mark.asyncio
async def test_request_json(httpbin, method, verb, json):
    """Test a request with the specified json."""
    _, echoed_response = await getattr(httpbin, method)(json=json)
    assert echoed_response.method == verb
    assert echoed_response.json == json


@pytest.mark.parametrize('params', [
    {},
    {'key': 'val'},
    {'key1': 'val1', 'key2': 'val2'},
])
@pytest.mark.parametrize('additional_params', [
    {},
    {'add_key': 'val'},
    {'key3': 'val1', 'key4': 'val2'},
])
@pytest.mark.asyncio
async def test_session_params(httpbin, params: Dict, additional_params: Dict):
    """Test a request with session params."""
    httpbin.add_params(**params)
    _, echoed_response = await httpbin.echo_request_get(
        params=additional_params)
    params.update(additional_params)
    assert echoed_response.params == params
    httpbin.clear_params()


@pytest.mark.parametrize('headers', [
    {},
    {'Key1': 'val1'},
    {'Key2': 'val2', 'Key3': 'val3'},
])
@pytest.mark.parametrize('additional_headers', [
    {},
    {'Key1': 'valA'},
    {'Key2': 'valB', 'Key3': 'valC'},
    {'Key4': 'val4'},
    {'Key5': 'val5', 'Key6': 'val6'},
])
@pytest.mark.asyncio
async def test_session_headers(headers: Dict, additional_headers: Dict):
    """Test a request with session headers."""
    httpbin = HttpBin(headers=headers)
    _, echoed_response = await httpbin.echo_request_get(
        headers=additional_headers)
    all_headers = {}
    all_headers.update(headers)
    all_headers.update(additional_headers)
    assert all_headers.items() <= echoed_response.headers.items()
    await httpbin.close()


@pytest.mark.asyncio
async def test_semaphore():
    """Ensure the semaphore limits the requests."""
    httpbin = HttpBin(simultaneous_requests=2)
    tasks = [httpbin.semaphore_test() for _ in range(0, 5)]
    results = await asyncio.gather(*tasks)
    assert len(list(filter(None, results))) == 2
