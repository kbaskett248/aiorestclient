import setuptools

setuptools.setup(
    name="aiorestclient",
    version="0.1.0",
    url="https://gitlab.com/kbaskett248/aiorestclient",

    author="Kenny Baskett",
    author_email="kbaskett248@gmail.com",

    description="A client library for simplifying interactions with RESTful services",
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(include='aiorestclient'),

    install_requires=[
        'aiohttp'
    ],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)
